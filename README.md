# demo-chatbot-jsoneditor

Editor JSON baseado nos padrões da SantoDigital

https://i.leonardo.gitlab.io/editor-json-chatbot-santodigital/

![main](https://gitlab.com/i.leonardo/editor-json-chatbot-santodigital/-/raw/master/main.png)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
