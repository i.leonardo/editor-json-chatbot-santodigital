// Create button <a> to download
export const createDownloadFile = (url, name) => {
  const link = window.URL.createObjectURL(url)
  // Construct the 'a' element
  const a = document.createElement('a')
  a.download = name
  a.target = '_blank'

  // Construct the URI
  a.href = link
  document.body.appendChild(a)
  a.click()

  // Cleanup the DOM
  document.body.removeChild(a)
  window.URL.revokeObjectURL(link)
}

export const readFile = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.onload = () => {
      resolve(reader.result)
    }

    reader.onerror = reject

    reader.readAsText(file)
  })
}
